#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return jsonify(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        j = 1
        done = 0
        insert_at = 0
        for i in range(len(books)):
            if int(books[i]['id']) != int(j):
                done = 1
                insert_at = i
                break
            j += 1

        if(done == 0):
            insert_at = len(books)
        
        title = request.form['name']
        id = j
        new_book_dict = {}
        for var in ["title", "id"]:
            new_book_dict[var] = eval(var)
        
        books.insert(insert_at, new_book_dict)
        return render_template('showBook.html', books = books)
    else:
	    # if my server didn't receive a POST request, it's going to 
	    # redirect the user back to the main index page.
	    # The helper function 'redirect' is used to accomplish that.
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        new_title = request.form['name']
        for i in books:
            if int(i.get("id")) == int(book_id):
                i["title"] = new_title
                break

        return render_template('showBook.html', books = books)
    else:
	    # if my server didn't receive a POST request, it's going to 
	    # redirect the user back to the main index page.
	    # The helper function 'redirect' is used to accomplish that.
	    return render_template('editBook.html', book_id = book_id)	


	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'GET':
        book_title = ''
        for i in books:
            if int(i.get("id")) == int(book_id):
                book_title = i.get("title")
                break
        return render_template('deleteBook.html', book_id = book_id, book_title = book_title)
    else:
        for i in range(len(books)):
            if int(books[i]['id']) == int(book_id):
                del books[i]
                break
        return render_template('showBook.html', books = books) 
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

